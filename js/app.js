const app = {
    button: document.querySelector(".app__content-share"),
    shareIcon: document.querySelector(".app__content-shareIcon"),
    shareCard: document.querySelector(".app__share-card")
};

app.shareCard.style.display = "none";

app.button.addEventListener('click', () => {
    if (app.shareCard.style.display === "none") {
        app.shareCard.style.display = "block";

    } else {
        app.shareCard.style.display = "none";
    }
})